# apartment-building-registration

This project is for exploring the City of Toronto's Open Data apartment building registration list.

Currently, this just documents the jq commands necessary for deriving data from the list. So you need jq to run the commands.

Data location: https://portal0.cf.opendata.inter.sandbox-toronto.ca/dataset/building-information-for-abs/

Command for finding buildings by property manager (i.e. akelius) and then pulling and formatting the addresses:
```
wget https://ckan0.cf.opendata.inter.sandbox-toronto.ca/download_resource/66837600-9f14-4031-b1f3-fd4bc0848522?format=json -O abrd.json
cat abrd.json | jq '.[]|select(.PROP_MANAGEMENT_COMPANY_NAME | try contains("AKELIUS")) | .SITE_ADDRESS' | sed 's/"//g' | awk '{ print $2 " " $3 " " $4 " " $5 " " $6 " " $7 " " $1}' | sed 's/\s\{2,\}/ /g' | sed 's/[A-Z]/\L&/g' | sed 's/\b\(.\)/\u\1/g' | sort > akelius_buildings.csv
```
